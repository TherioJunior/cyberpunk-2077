# Cyberpunk 2077

This is a repository where I note down all sorts of shit related to Cyberpunk, mainly in regards to modding/QoL improvements.

## Prerequisites

Install Cyber Engine Tweaks: [GitHub](https://github.com/maximegmd/CyberEngineTweaks)

Download the complete Icon list: [here](./Item%20list.xlsx)

##### Optional, but recommended:

Have an Excel Editor installed. If you don't want to or can't for some reason, there'd be [this](https://products.aspose.app/cells/editor) or also [this](https://products.groupdocs.app/editor/excel), both of which require no registration.

For the list provided on this repository (originally published on NexusMods [here](https://www.nexusmods.com/cyberpunk2077/mods/521)), either Excel by Microsoft themselves or LibreOffice which you can get [here](https://www.libreoffice.org/) is recommended.

## Commands and QoL improvements

Head over to the [Misc](./Misc.md) file.

## Scripts

The bin/... folder contains scripts & it's prepared so that after cloning this repository you could just copy paste that folder into your Cyberpunk 2077 installation folder and [Cyber Engine Tweaks](https://github.com/maximegmd/CyberEngineTweaks) would automatically detect it.

Once ingame, scripts can be executed using the following command:

```lua
dofile('FILE NAME HERE')
```

- [quest-skip.lua](./bin/x64/plugins/cyber_engine_tweaks/mods/quest-skip.lua)
  
  - This is a script which automatically completes the currently selected Mission from your Journal.
  
  - **<u>BEWARE</u>** This script can break your game. For example; when skipping the Phantom Liberty mission of the main storyline, and you got the "True Ending" (won't spoiler) you won't be able to enter Dogtown anymore, and fast traveling inside won't show up any Vendors. It is safe to use with most missions in case you're stuck though.
