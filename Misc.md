# Cyberpunk 2077 - Misc

This is a collection of commands which might prove useful to you depending on what you want to do. I may or may not expand this list further as time goes on.

## Cyberpunk 2077 v2.0 Patch disclaimer

The v2.0 Patch of Cyberpunk removed the following (maybe there are more) functions from the game:

- `Game.GiveDevPoints`

- `Game.SetAtt`

- `Game.ApplyEffectOnPlayer`

- `Game.ModStatPlayer`

- `Game.SetDebugFact`

I haven't found an alternative to the other commands yet, but some people including myself have found an alternative to `Game.SetDebugFact`, which is as follows:

```lua
Game.GetQuestsSystem():SetFactStr("DEBUG FACT HERE", 1)
```

This works or should work with pretty much all `Game.SetDebugFact` commands you find in outdated articles and posts.

To manually check the value of Debug Facts yourself, you can use the following command:

```lua
print(Game.GetQuestsSystem():GetFactStr("DEBUG FACT HERE"))
```

## Unlock Secret Ending with Johnny

This will unlock a "Secret Ending" with Johnny in the main storyline, by manually setting your bond with him to above 70%. Normally this'd depend on which dialogue options you chose at certain points of the game, but this lets you experience the ending (provided you have an old save laying around) no matter which dialogue options you chose throughout the story.

```lua
Game.GetQuestsSystem():SetFactStr("sq032_johnny_friend", 1)
```

## Change Skippy's firing mode

Skippy is kind of a troll weapon. No matter which mode you choose at first, sooner or later (after 50 kills, though this can vary as stated in various posts on Reddit and Steam) it'll switch to the mode you haven't selected and there's nothing you can do about it. That is unless you manually modify the firing mode using [Cyber Engine Tweaks](https://github.com/maximegmd/CyberEngineTweaks).

To change Skippy into "**Stone-Cold Killer**" mode (headshot only targetting, lethal):

```lua
Game.GetQuestsSystem():SetFactStr("mq007_skippy_aim_at_head", 1)
```

To change Skippy into "**Puppy-Loving Pacifist**" mode (leg only targetting, non lethal):

```lua
Game.GetQuestsSystem():SetFactStr("mq007_skippy_aim_at_head", 0)
```

## Unlock all Twitch Prime drops

So I don't have Twitch Prime, but the weapons and especially the outfit that come from it are kind of cool, so I went looking and on accident found some Twitter Post detailing commands to get all **12** Twitch Prime rewards, which as far as I know and as of the point of writing this, aren't even obtainable yet (at least not all of them).

```lua
Game.AddToInventory("Items.Preset_Ashura_Twitch",1)
Game.AddToInventory("Items.Twitch_Drop_Pants", 1)
Game.AddToInventory("Items.Twitch_Drop_Vest", 1)
Game.AddToInventory("Items.Twitch_Drop_Boots", 1)
Game.AddToInventory("Items.Twitch_Drop_Specs",1)
Game.AddToInventory("Items.Preset_Warden_Amazon", 1)
Game.AddToInventory("Items.Preset_Nekomata_Amazon", 1)
Game.AddToInventory("Items.Preset_Kyubi_Amazon", 1)
Game.AddToInventory("Items.Preset_Grit_Amazon", 1)
Game.AddToInventory("Items.Preset_Crusher_Amazon", 1)
Game.AddToInventory("Items.Preset_Ajax_Amazon", 1)
Game.AddToInventory("Items.Preset_Ticon_Gwent", 1)
```
